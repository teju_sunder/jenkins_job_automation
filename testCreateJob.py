import jenkins
import xml.etree.ElementTree as etree
import json

#JSON Config File
with open('/Users/apple/PycharmProjects/jenkinstest/testCreateJob.json') as data_file:
    dataj = json.loads(data_file.read())
# Establishing Connection To Jenkins
server = jenkins.Jenkins(url=dataj[0]['url'],username=dataj[0]['user'],password=dataj[0]['apiToken'])
#Parsing The XML File
e = etree.parse(dataj[0]['Path'])
root = e.getroot()
#Getting All The Microservices In The Loop
for child in root:
#Converting XML To String
    data = etree.tostring(child)
#Getting The Firstline And Removing The Child Name
    jobname = data.split("\n")[0]
    replace = data.replace(jobname,"")
#Getting The Last Line And Removing The Child Name
    jobnamelast = replace.split()[-1]
    replace1 = replace.replace(jobnamelast,"")
#Removing "<"">" To Get The Job Name
    name = jobname.replace("<","")
    name = name.replace(">","")
#Creating a Jenkins Job
    server.create_job(name,replace1 )


